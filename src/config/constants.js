const devConfig = {
  MONGO_URL: 'mongodb://localhost:27017/todolist-dev',
};

const testConfig = {
  MONGO_URL: 'mongodb://mongo:27017/todolist-test',
};

const prodConfig = {
  MONGO_URL: 'mongodb+srv://todoback:cYQ2kHK0e0AdTuf5@todo-prod.qvxtf.mongodb.net/todolist-prod?retryWrites=true&w=majority',
};

const defaultConfig = {
  PORT: 5321,
  JWT_SECRET: 't0ps3cr3t!',
};

function envConfig(env) {
  switch (env) {
    case 'dev':
      return devConfig;
    case 'test':
      return testConfig;
    default:
      return prodConfig;
  }
}

export default {
  ...defaultConfig,
  ...envConfig(process.env.NODE_ENV),
};
